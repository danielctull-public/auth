
import Foundation

struct Credential: Codable {
    let username: String
    let password: String
}

extension Credential {

    var authorizationHeader: String? {
        let base = "\(username):\(password)"
        guard let data = base.data(using: .utf8) else { return nil }
        let encoded = data.base64EncodedString()
        return "Basic \(encoded)"
    }
}
