
import Auth
import Foundation

struct Account: Auth.Account, Codable {

    typealias Credential = BasicAuth.Credential

    public let name: String
    public let identifier: String
    public init(identifier: String, name: String) {
        self.identifier = identifier
        self.name = name
    }

    func sign(request: URLRequest) -> URLRequest {

        guard let header = credential?.authorizationHeader else {
            return request
        }
        var mutable = request
        mutable.addValue(header, forHTTPHeaderField: "Authorization")
        return mutable
    }
}
