
import Foundation

public protocol Account: Codable {

    associatedtype Credential: Codable

    var identifier: String { get }
    var name: String { get }

    func sign(request: URLRequest) -> URLRequest
}

extension Account {

    public var credential: Credential? {
        let decoder = JSONDecoder()
        return try? decoder.decode(Credential.self, from: Data())
    }
}
