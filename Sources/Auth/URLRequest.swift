
import Foundation

extension URLRequest {

    public mutating func sign<A: Account>(with account: A) {
        self = account.sign(request: self)
    }
}
