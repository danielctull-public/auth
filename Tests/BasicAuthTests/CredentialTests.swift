
@testable import BasicAuth
import XCTest

final class CredentialTests: XCTestCase {

    func testAuthorisationHeader() {
        let credential = Credential(username: "daniel", password: "password")
        XCTAssertEqual(credential.authorizationHeader, "Basic ZGFuaWVsOnBhc3N3b3Jk")
    }
}
