import XCTest

import BasicAuthTests

var tests = [XCTestCaseEntry]()
tests += BasicAuthTests.__allTests()

XCTMain(tests)
