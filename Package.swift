// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "Auth",
    products: [
        .library(
            name: "Auth",
            targets: ["Auth"]),
        .library(
            name: "BasicAuth",
            targets: ["BasicAuth"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "Auth",
            dependencies: []),
        .testTarget(
            name: "AuthTests",
            dependencies: ["Auth"]),

        .target(
            name: "BasicAuth",
            dependencies: ["Auth"]),
        .testTarget(
            name: "BasicAuthTests",
            dependencies: ["BasicAuth"]),
    ]
)
